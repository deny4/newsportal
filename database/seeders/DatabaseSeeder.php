<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Theme;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Admin::insert([
             'name' => 'Super admin',
             'email' => 'daniel.koubek@centrum.cz',
             'password' => bcrypt('123456a_*'),
             'image' => '',
        ]);

        Theme::insert([
            'site_title' => 'Deny newsportal'   
        ]);
    }
}
