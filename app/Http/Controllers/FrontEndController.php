<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Theme;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function index() {
        $latest_news = News::latest()->where('status', 1)->take(3)->get();
        $theme = Theme::first();
        return view('index', compact('latest_news', 'theme'));
    }
}
